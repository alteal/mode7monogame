﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System;

namespace SimpleCities
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>

    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D background;
        Texture2D tree;
        Camera camera;
        List<Vector2> trees = new List<Vector2>();
        Mode7 mode7;
        bool pressed;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            IsMouseVisible = true;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            mode7 = new Mode7(GraphicsDevice, Content);
            camera = new Camera(GraphicsDevice.Viewport);

            background = Content.Load<Texture2D>("Textures/maps/map");
            tree = Content.Load<Texture2D>("Textures/sprites/tree");            

            // TODO: use this.Content to load your game content here
        }

        public void BrushFoliage(float x, float y)
        {
            trees.Add(new Vector2(x, y));
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            MouseState mouse = Mouse.GetState();

            trees.Sort(SortByY);

            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                mode7.Zoom(0.001f * (float)gameTime.ElapsedGameTime.TotalMilliseconds);
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                mode7.Zoom(-0.001f * (float)gameTime.ElapsedGameTime.TotalMilliseconds);
            }

            camera.Update(gameTime);

            if (mouse.LeftButton == ButtonState.Pressed)
            {
                pressed = true;
            }

            if (mouse.LeftButton == ButtonState.Released && pressed == true)
            {
                Vector2 position = camera.TranslateMouse(new Vector2(mouse.Position.X, mouse.Position.Y));
                trees.Add(position);
                pressed = false;
            }

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        public int SortByY(Vector2 a, Vector2 b)
        {
            return Vector2.Transform(a, camera.GetMatrix()).Y.CompareTo(Vector2.Transform(b, camera.GetMatrix()).Y);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {

            mode7.Begin(spriteBatch);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, camera.GetMatrix());

            spriteBatch.Draw(background, new Vector2(0, 0), origin: new Vector2(background.Width / 2, background.Height / 2));

            spriteBatch.End();
            mode7.End(spriteBatch);

            //spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null, camera.GetMatrix());
            spriteBatch.Begin(SpriteSortMode.Immediate);

            foreach (Vector2 position in trees)
            {
                Vector2 transformed = Vector2.Transform(position, camera.GetMatrix());
                Vector2 translated = mode7.TranslateCoords(transformed);
                float scale = mode7.TranslateScale(transformed.Y);
                spriteBatch.Draw(tree, translated, color: Color.White, origin: new Vector2(tree.Width / 2, tree.Height * .9f), scale: new Vector2(scale, scale));

                //position.Draw(spriteBatch, 0, position, new Vector2(scale, scale));
            }

            spriteBatch.End();

            // TODO: Add your drawing code here

            //base.Draw(gameTime);
        }
    }
}
