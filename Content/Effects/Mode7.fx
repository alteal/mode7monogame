﻿sampler s0;
float intensity;

float4 PixelShaderFunction(float2 coords: TEXCOORD0) : COLOR0
{
	if (intensity >= 1)
	{
	    return tex2D(s0, coords.xy);
	}

	float scale = intensity + lerp(0.0, 1 - intensity, coords.y);

	float sectors = 1 / scale;
	float offset = sectors / 2 - sectors / 2 / sectors;
	coords.x = coords.x / scale - offset;

	coords.y = coords.y / scale;

	coords.x = coords.x * intensity + (0.5 - lerp(0, .5, intensity));

	float4 Color;
	Color = tex2D(s0 , coords.xy);
	return Color;
}
technique Technique1
{
    pass Pass1
    {
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}