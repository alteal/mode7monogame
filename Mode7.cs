﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace SimpleCities
{
    class Mode7
    {
        Effect mode7;
        float mode7Zoom = 1;
        RenderTarget2D render;

        public Mode7(GraphicsDevice graphicsDevice, ContentManager content)
        {
            mode7 = content.Load<Effect>("Effects/Mode7");
            render = new RenderTarget2D(graphicsDevice, graphicsDevice.Viewport.Width, graphicsDevice.Viewport.Height);
        }

        public void Zoom(float amount)
        {
            mode7Zoom += amount;

            if (mode7Zoom > 1)
            {
                mode7Zoom = 1;
            }

            if (mode7Zoom < 0)
            {
                mode7Zoom = 0;
            }

            Console.WriteLine("Mode7Zoom: " + mode7Zoom);
        }

        public Vector2 TranslateCoords(Vector2 location)
        {
            float x = location.X / render.Width;
            float y = location.Y / render.Height;

            y = y + (1 - 2 * y);

            float scale = mode7Zoom + MathHelper.Lerp(0.0f, 1 - mode7Zoom, y);
            y = (1 - y / scale);

            float sectors = 1 / scale;
            float offset = sectors / 2 - sectors / 2 / sectors;
            x = x / scale - offset;

            y *= render.Height;
            x *= render.Width;

            return new Vector2(x, y);
        }


        public float TranslateScale(float yPos)
        {
            float y = yPos / render.Height;
            y = y + (1 - 2 * y);
            float scale =  1 / (mode7Zoom + MathHelper.Lerp(0.0f, 1 - mode7Zoom, y));
            return scale;
        }

        public void Begin(SpriteBatch spriteBatch)
        {
            spriteBatch.GraphicsDevice.SetRenderTarget(render);
            spriteBatch.GraphicsDevice.Clear(Color.CornflowerBlue);
        }

        public void End(SpriteBatch spriteBatch)
        {
            spriteBatch.GraphicsDevice.SetRenderTarget(null);
            spriteBatch.Begin(SpriteSortMode.Immediate, null, null, null, null, null);
            mode7.Parameters["intensity"].SetValue(mode7Zoom);
            mode7.CurrentTechnique.Passes[0].Apply();
            spriteBatch.Draw(render, new Vector2(0, 0), Color.White);
            spriteBatch.End();
        }
    }
}
