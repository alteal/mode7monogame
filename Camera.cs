﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace SimpleCities
{
    class Camera
    {
        Viewport viewport;

        float rotationSpeed = 0.001f;
        public float Rotation { get; private set; } = 0;

        float moveSpeed = 0.1f;
        public Vector2 Position { get ; private set; }

        float zoom = 1;
        float zoomSpeed = 0.05f;

        bool cameraChanged = false;

        Matrix lookAt;

        public Matrix Matrix { get; private set; }

        public Camera(Viewport viewport)
        {
            this.Position = new Vector2(0, 0);
            this.viewport = viewport;
            lookAt = Matrix.CreateTranslation(new Vector3(viewport.Width / 2, viewport.Height / 2, 0));
            SetMatrix();
        }

        public void SetMatrix()
        {
            this.Matrix =
                Matrix.CreateTranslation(Position.X, Position.Y, 0) *
                Matrix.CreateRotationZ(Rotation) *
                Matrix.CreateScale(zoom) *
                lookAt;
        }

        public Matrix GetMatrix()
        {
            if (cameraChanged)
            {
                SetMatrix();
                cameraChanged = false;
            }

            return this.Matrix;
        }

        public Vector2 TranslateMouse(Vector2 mousePosition)
        {
            float camX =  viewport.Width / 2;
            float camY =  viewport.Height / 2;

            Matrix matrix = Matrix.Identity * Matrix.CreateRotationZ(-Rotation);

            Vector2 transformed = Vector2.Transform(mousePosition - new Vector2(camX, camY), matrix) + new Vector2(-Position.X, -Position.Y);
            return transformed;
        }

        public void Update(GameTime gameTime)
        {
            RotateCamera(gameTime);
            MoveCamera(gameTime);
        }

        private void RotateCamera(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Q))
            {
                cameraChanged = true;
                Rotation -= rotationSpeed * gameTime.ElapsedGameTime.Milliseconds;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.E))
            {
                cameraChanged = true;
                Rotation += rotationSpeed * gameTime.ElapsedGameTime.Milliseconds;
            }
        }

        private void MoveCamera(GameTime gameTime)
        {
            float y = 0;
            float x = 0;

            if (Keyboard.GetState().IsKeyDown(Keys.W))
                y = 1;

            if (Keyboard.GetState().IsKeyDown(Keys.S))
                y = -1;

            if (Keyboard.GetState().IsKeyDown(Keys.D))
                x = 1;

            if (Keyboard.GetState().IsKeyDown(Keys.A))
                x = -1;

            if (x != 0 || y != 0)
            {
                float movDir = (float)Math.Atan2(x, y);
                x = Position.X - (float)(Math.Sin(movDir - Rotation) * moveSpeed * gameTime.ElapsedGameTime.TotalMilliseconds);
                y = Position.Y + (float)(Math.Cos(movDir - Rotation) * moveSpeed * gameTime.ElapsedGameTime.TotalMilliseconds);
                Position = new Vector2(x, y);
                cameraChanged = true;
            }
        }
    }
}
